-- MySQL dump 10.13  Distrib 5.5.57, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: guestbook
-- ------------------------------------------------------
-- Server version	5.5.57-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `datetime` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `message` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES ('19:03:16 11.11.2018','Sveta','sveta@mail.ru','leave the message'),('19:22:58 11.11.2018','Anonymous','anonym@gmail.com','Anonymous message. Have a nice day!'),('19:54:46 11.11.2018','Grigorii','gdolomanji@gmail.com','another test message');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guest_counter`
--

DROP TABLE IF EXISTS `guest_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guest_counter` (
  `datetime` varchar(45) DEFAULT NULL,
  `device` varchar(45) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `browser` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guest_counter`
--

LOCK TABLES `guest_counter` WRITE;
/*!40000 ALTER TABLE `guest_counter` DISABLE KEYS */;
INSERT INTO `guest_counter` VALUES ('18:59:14 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('18:59:16 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('18:59:20 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('18:59:29 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:02:59 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:15:33 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:16:06 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:16:15 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:18:26 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:22:17 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:22:58 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:23:41 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:23:42 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:25:45 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:54:46 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph'),('19:57:48 11.11.2018','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63','188.237.250.142','php-labs-univer-demchukova.c9users.io/lab2-ph');
/*!40000 ALTER TABLE `guest_counter` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-11 20:09:00
